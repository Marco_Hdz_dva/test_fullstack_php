 /* var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 19.3842437, lng: -99.2358031},
          zoom: 10
        });
      }*/


function initMap(lati,long,tienda,zoomin,locations) {
    
    	lati = 19.3842437;
    	long = -99.2358031;
        zoomin = 10;
    
    
        $.get( "/getConstrucciones", {  } ,function( data ) {
    		   var locations = [];
    		  obj = JSON.parse(data);
               for (var i = 0; i < obj.length; i++) {
                    var latitud = obj[i].latitud;
                    latitud = latitud.replace("°", "");
                    var longitud = obj[i].longitud;
                    longitud = longitud.replace("°", "");
                    locations[i]= ([obj[i].nombre,latitud, longitud,i]);
                }
                 setTimeout(mapa(lati,long,tienda,zoomin,locations), 2000);
        });
    
}
function mapa(lati,long,tienda,zoomin,locations){
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: zoomin,
      center: new google.maps.LatLng(lati,long),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
      var infowindow = new google.maps.InfoWindow();
    
        var marker, i;
    
        for (i = 0; i < locations.length; i++) {  
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
          });
    
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
    
}