$(document).ready(function(){
    $('.tabs').tabs();
    $('.collapsible').collapsible();
    $('#login').click(function() {
        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax({
            url: "/login/"+email+"/"+password,
            success: function (response) {
            var infoUser = jQuery.parseJSON( response );
                if(infoUser.success){
                    window.location.href = "/dashboard";
                }else{
                   $('#error').html(infoUser.error);
                }

            },error: function (response) {

            },
        });
    });
    $('.btn.update').click(function() {
        $('#listado').css('display','none');
        $('#listado').removeClass('active');
        $('#alta').css('display','block');
        $('#alta').addClass('active');
        $(".indicator").css('left','332px');
        $(".indicator").css('right','663px');
        var id = $(this).attr("id").split("_");
        $.ajax({
            url: "/getConstruccion/"+id[1],
            success: function (response) {
            var construccion = jQuery.parseJSON( response );
                $("label").addClass('active');
                var clave = construccion.clave;
                var clv = clave.split("/");
                clave = clv[1];
                $('#clave').val(clave);
                $('#nombre').val(construccion.nombre);
                $('#delegacion').val(construccion.delegacion);
                $('#colonia').val(construccion.colonia);
                $('#calle').val(construccion.calle);
                $('#longitud').val(construccion.longitud);
                $('#latitud').val(construccion.latitud);
                $('#tipo').val("update");
                $('#id').val(construccion.id);
                
                

            },error: function (response) {

            },
        });
    });
    $('.btnCarac').click(function() {
        var id = $(this).attr("id").split("_");
        var caract = $("#caracteristica_"+id[1]).val();
        $.ajax({
            url: "/addCaracteristica/"+id[1]+"/"+caract,
            success: function (response) {
            var caracteristica = jQuery.parseJSON( response );
                for (var i = 0; i < caracteristica.length; i++) {
                   $('.ulContent_'+id[1]).append("<li>"+caracteristica[i].descripcion+"</li>");    
                }
            },error: function (response) {
                
            },
        });
    });
    $('.articulo').click(function() {
        var id = $(this).attr("id").split("_");
        $('.ulContent_'+id[1]).html("");
        $.ajax({
            url: "/getCaracteristicas/"+id[1],
            success: function (response) {
            var caracteristica = jQuery.parseJSON( response );
                for (var i = 0; i < caracteristica.length; i++) {
                    $('.ulContent_'+id[1]).append("<li>"+caracteristica[i].descripcion+"</li>");    
                }
            },error: function (response) {
                
            },
        });
    });
    
});