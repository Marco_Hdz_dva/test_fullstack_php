<?php

namespace PropiedadesBundle\Controller;

use PropiedadesBundle\Entity\Users;
use PropiedadesBundle\Entity\Caracteristicas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class AjaxController extends Controller
{
    
    public function loginAction($email,$password)
    {
        $em = $this->getDoctrine()->getManager();
            $usuario = $em->getRepository('PropiedadesBundle:Users')->findBy(array('email'=>$email,'password'=>$password));
                if($usuario){
                    $response = true;
                     return new Response(json_encode(array('success'=>$response)));
                }else{
                    $response = "El email o contraseña no concuerdan";        
                     return new Response(json_encode(array('error'=>$response)));
                }
       
    }
    public function getConstrucctionAction($id)
    {
        $em = $this->getDoctrine()->getManager();
            $construccion = $em->getRepository('PropiedadesBundle:Construcciones')->findBy(array('id'=>$id));
            $response = array('clave'=>$construccion[0]->getClave(),
                              'nombre'=>$construccion[0]->getNombre(),
                              'delegacion'=>$construccion[0]->getDelegacion(),
                              'colonia'=>$construccion[0]->getColonia(),
                              'calle'=>$construccion[0]->getCalle(),
                              'latitud'=>$construccion[0]->getLatitud(),
                              'id'=>$construccion[0]->getId(),
                              'longitud'=>$construccion[0]->getLongitud());
        return new Response(json_encode($response));
    }
    public function addCaracteristicaAction($id,$caract)
    {
            $em = $this->getDoctrine()->getManager();
            $caracteristicas = new Caracteristicas();
            $caracteristicas->setIdConstruccion($id);
            $caracteristicas->setDescripcion($caract);
            $em->persist($caracteristicas);
            $em->flush();
    
            $caracteristicas = $em->getRepository('PropiedadesBundle:Caracteristicas')->findBy(array('idConstruccion'=>$id));
            $response = array();
            foreach($caracteristicas as $caracteristica) {
            $response[]  = array('descripcion'=>$caracteristica->getDescripcion());
            }
        return new Response(json_encode($response));
    }
    public function getCaracteristicasAction($id)
    {
        $em = $this->getDoctrine()->getManager();
            $caracteristicas = $em->getRepository('PropiedadesBundle:Caracteristicas')->findBy(array('idConstruccion'=>$id));
            $response = array();
            foreach($caracteristicas as $caracteristica) {
            $response[]  = array('descripcion'=>$caracteristica->getDescripcion());
            }
        return new Response(json_encode($response));
    }
    public function getConstrucctionesAction()
    {
        $em = $this->getDoctrine()->getManager();
            $construcciones = $em->getRepository('PropiedadesBundle:Construcciones')->findAll();
            //secho count ($construcciones);
        $response = array();
            foreach($construcciones as $construccion) {
            $response[]  = array('clave'=>$construccion->getClave(),
                              'nombre'=>$construccion->getNombre(),
                              'delegacion'=>$construccion->getDelegacion(),
                              'colonia'=>$construccion->getColonia(),
                              'calle'=>$construccion->getCalle(),
                              'id'=>$construccion->getId(),
                              'latitud'=>$construccion->getLatitud(),
                              'longitud'=>$construccion->getLongitud());
            }
        return new Response(json_encode($response));
    }
}
