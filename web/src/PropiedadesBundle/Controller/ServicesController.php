<?php

namespace PropiedadesBundle\Controller;

use PropiedadesBundle\Entity\Users;
use PropiedadesBundle\Entity\Caracteristicas;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ServicesController extends Controller
{
    
    public function AenusAction($longitud,$latitud)
    {
       $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.foursquare.com/v2/venues/explore?client_id=3MREIOPWHWYQN1KF3QSYSSJW4INE3VR1PS4T5LU4F3PN0EKQ&client_secret=RIEGMLBZZ33ZO4VLNIT1MCK5ENBTJMG2F2H5A4M3A1B4ASH1&v=20180323&ll=".$latitud.",".$longitud.,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {
              return "cURL Error #:" . $err;
            } else {
              return $response;
            }
       
    }
    public function getPhotosAction($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.foursquare.com/v2/venues/".$id."/photos?client_id=3MREIOPWHWYQN1KF3QSYSSJW4INE3VR1PS4T5LU4F3PN0EKQ&client_secret=RIEGMLBZZ33ZO4VLNIT1MCK5ENBTJMG2F2H5A4M3A1B4ASH1&v=20180730",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }
    
}
