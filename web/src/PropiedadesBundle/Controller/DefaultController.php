<?php

namespace PropiedadesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PropiedadesBundle:Default:index.html.twig');
    }
}
