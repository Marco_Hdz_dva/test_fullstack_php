<?php

namespace PropiedadesBundle\Controller;


use PropiedadesBundle\Entity\Users;
use PropiedadesBundle\Entity\Construcciones;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\EntityManager;

class HomeController extends Controller
{
    public function indexAction()
    {
     
        return $this->render('@Propiedades/Home/index.html.twig');
    }
    public function registrateAction()
    {
        return $this->render('@Propiedades/Home/registrate.html.twig');
    }
    public function guardarRegistroAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $first_name =  $request->query->get('first_name');
        $last_name = $request->query->get('last_name');
        $email = $request->query->get('email');
        $password = $request->query->get('password');
        $usuario = new Users();
        $usuario->setFirstName($first_name);
        $usuario->setLastName($last_name);
        $usuario->setEmail($email);
        $usuario->setPassword($password);
        $em->persist($usuario);
        $em->flush();
        //crear sesion y enviar al dashboard
        return $this->redirectToRoute('propiedades_dashboard');
    }
    public function guardarConstruccionAction(Request $request)
    {
        if($request->query->get('tipo')=="nuevo"){
            $em = $this->getDoctrine()->getManager();
            $nombre =  $request->query->get('nombre');
            $clave = "PCOM-XXX/".$request->query->get('clave');
            $delegacion = $request->query->get('delegacion');
            $colonia = $request->query->get('colonia');
            $calle = $request->query->get('calle');
            $latitud = $request->query->get('latitud');
            $longitud = $request->query->get('longitud');
            $construcciones = new Construcciones();
            $construcciones->setNombre($nombre);
            $construcciones->setClave($clave);
            $construcciones->setDelegacion($delegacion);
            $construcciones->setColonia($colonia);
            $construcciones->setCalle($calle);
            $construcciones->setLongitud($latitud);
            $construcciones->setLatitud($longitud);
            $em->persist($construcciones);
            $em->flush();    
        }else{
            $em = $this->getDoctrine()->getManager();
            $id =  $request->query->get('id');
            $nombre =  $request->query->get('nombre');
            $clave = "PCOM-XXX/".$request->query->get('clave');
            $delegacion = $request->query->get('delegacion');
            $colonia = $request->query->get('colonia');
            $calle = $request->query->get('calle');
            $latitud = $request->query->get('latitud');
            $longitud = $request->query->get('longitud');
            $construcciones = $em->getRepository('PropiedadesBundle:Construcciones')->find($id);
            $construcciones->setNombre($nombre);
            $construcciones->setClave($clave);
            $construcciones->setDelegacion($delegacion);
            $construcciones->setColonia($colonia);
            $construcciones->setCalle($calle);
            $construcciones->setLongitud($latitud);
            $construcciones->setLatitud($longitud);
            $em->persist($construcciones);
            $em->flush();
        }
        
        //crear sesion y enviar al dashboard
        return $this->redirectToRoute('propiedades_dashboard');
    }
    public function borrarConstrucctionAction($id)
    {
            $em = $this->getDoctrine()->getManager();
            $construcciones = $em->getRepository('PropiedadesBundle:Construcciones')->find($id);
            $em->remove($construcciones);
            $em->flush();
        return $this->redirectToRoute('propiedades_dashboard');
    }
    public function dashboardAction()
    {
         $em = $this->getDoctrine()->getManager();
        $construcciones = $em->getRepository('PropiedadesBundle:Construcciones')->findAll();
        $data = array( 'construcciones' => $construcciones);
        //$template = sprintf('PropiedadesBundle:Home:dashboard.html.twig');
        //return $this->container->get('templating')->renderResponse($template,$data);
        return $this->render('@Propiedades/Home/dashboard.html.twig',$data);
    }
}
