<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Caracteristicas
 *
 * @ORM\Table(name="caracteristicas")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\CaracteristicasRepository")
 */
class Caracteristicas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idConstruccion", type="integer")
     */
    private $idConstruccion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idConstruccion
     *
     * @param integer $idConstruccion
     *
     * @return Caracteristicas
     */
    public function setIdConstruccion($idConstruccion)
    {
        $this->idConstruccion = $idConstruccion;

        return $this;
    }

    /**
     * Get idConstruccion
     *
     * @return int
     */
    public function getIdConstruccion()
    {
        return $this->idConstruccion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Caracteristicas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}

