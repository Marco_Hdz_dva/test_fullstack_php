<?php

/* @Propiedades/Home/dashboard.html.twig */
class __TwigTemplate_1c9081768ce915510a2d71a964de0c72d10f96d6ce854cfd2d2febd44ada7d22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Propiedades/Home/dashboard.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Propiedades/Home/dashboard.html.twig"));

        // line 1
        echo "<html lang=\"es\">
    <head>
        <title>Propiedades.com examén </title>
        ";
        // line 4
        $this->displayBlock('style', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('script', $context, $blocks);
        // line 15
        echo "    </head>
   
    <body>
        <section class=\"dashboard\">
        <div class=\"row\">
            <div class=\"col s12\">
              <ul class=\"tabs\">
                <li class=\"tab col s3\"><a href=\"#listado\">Listado de construcciones</a></li>
                <li class=\"tab col s3\"><a href=\"#alta\">Alta de construcción</a></li>
                <li class=\"tab col s3\"><a href=\"#mapa\">Mapa de construcciones</a></li>
              </ul>
            </div>
            <div id=\"listado\" class=\"col s12\">
                <div class=\"row\">
                    <div class=\"col s3\">Clave</div>
                    <div class=\"col s3\">Nombre</div>
                    <div class=\"col s3\">Delegacion</div>
                    <div class=\"col s3\"></div>
                </div>
                ";
        // line 34
        if ((isset($context["construcciones"]) ? $context["construcciones"] : $this->getContext($context, "construcciones"))) {
            // line 35
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["construcciones"]) ? $context["construcciones"] : $this->getContext($context, "construcciones")));
            foreach ($context['_seq'] as $context["_key"] => $context["construccion"]) {
                // line 36
                echo "                                ";
                if ($context["construccion"]) {
                    // line 37
                    echo "                                  
                                  <ul class=\"collapsible\">
                                    <li>
                                      <div class=\"collapsible-header\">
                                          <div class=\"col s3\">";
                    // line 41
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "clave", array()), "html", null, true);
                    echo "</div>
                                          <div class=\"col s3\">";
                    // line 42
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "nombre", array()), "html", null, true);
                    echo "</div>
                                          <div class=\"col s3\">";
                    // line 43
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "delegacion", array()), "html", null, true);
                    echo "</div>
                                          <div class=\"col s3 articulo\" id=\"articulo_";
                    // line 44
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\" >Ver más</div>
                                      </div>
                                      <div class=\"collapsible-body\">
                                        <article>
                                            <ul class=\"ulDescripcion\">
                                                <li>Nombre: ";
                    // line 49
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "nombre", array()), "html", null, true);
                    echo "</li>
                                                <li>Dirección: ";
                    // line 50
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "calle", array()), "html", null, true);
                    echo ", ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "colonia", array()), "html", null, true);
                    echo ",";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "delegacion", array()), "html", null, true);
                    echo ".</li>
                                                <li>Galeria de imagenes</li>
                                                <li>
                                                    <div class=\"btn update\" id=\"id_";
                    // line 53
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\">Actualizar</div>
                                                    <div class=\"btn \"><a href=\"/borrarConstruccion/";
                    // line 54
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\">Eliminar</a></div>
                                                </li>
                                            </ul>
                                            <ul class=\"ulCaracteristicas\">
                                                <li class=\"col s12\">Caracteristicas</li>
                                                <li class=\"col s12\">
                                                    <div class=\"col s6 input-field\">
                                                    <input id=\"caracteristica_";
                    // line 61
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\" name=\"caracteristica_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\" type=\"text\" class=\"validate\">
                                                    <label for=\"caracteristica_";
                    // line 62
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\">Caracteristicas de la construcción</label>
                                                    </div>
                                                    <div class=\"btn col s2 btnCarac\" id=\"btn_";
                    // line 64
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\">Agregar</div>
                                                </li>
                                                <li class=\"col s12 contentCaract\">
                                                    <ul class=\"ulContent_";
                    // line 67
                    echo twig_escape_filter($this->env, $this->getAttribute($context["construccion"], "id", array()), "html", null, true);
                    echo "\">
                                                       
                                                    </ul>
                                                </li>
                                            </ul>
                                        </article>
                                      </div>
                                    </li>

                                  </ul>
                                ";
                }
                // line 78
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['construccion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                        ";
        }
        // line 80
        echo "            </div>
                <div class=\"col s2\"></div>
                <div id=\"alta\" class=\"col s8\">
                    <form class=\"\" name=\"registro_construccion\" action=\"/guardar_construccion\">
                    <div class=\"input-field col s6\">
                      <input id=\"nombre\" name=\"nombre\" type=\"text\" class=\"validate\">
                      <label for=\"nombre\">Nombre</label>
                    </div>
                    <div class=\"input-field col s6\">
                      <input id=\"clave\" name=\"clave\" type=\"text\" class=\"validate\">
                      <label for=\"clave\">Clave</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"delegacion\" name=\"delegacion\" type=\"text\" class=\"validate\">
                      <label for=\"delegacion\">Delegación</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"colonia\" name=\"colonia\" type=\"text\" class=\"validate\">
                      <label for=\"colonia\">Colonia</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"calle\" name=\"calle\" type=\"text\" class=\"validate\">
                      <label for=\"calle\">Calle</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"longitud\" name=\"longitud\" type=\"text\" class=\"validate\">
                      <label for=\"longitud\">Longitud</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"latitud\" name=\"latitud\" type=\"text\" class=\"validate\">
                      <label for=\"latitud\">Latitud</label>
                    </div>
                    <div class=\"input-field col s12 center\">
                      <input type=\"hidden\" id=\"tipo\" name=\"tipo\" value=\"nuevo\">
                      <input type=\"hidden\" id=\"id\" name=\"id\" value=\"\">
                      <input type=\"submit\" value=\"Guardar\">  
                    </div>
                    </form>
                </div>
            
            <div id=\"mapa\" class=\"col s8\">
                 <div id=\"map\"></div>
            </div>
          </div>
        </section>
    </body>
</html>

<script type=\"text/javascript\" src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/maps.js"), "html", null, true);
        echo "\"></script>
<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDNXSa8poK_u1zqqRfmCcu9yJ1IamYJzsA&callback=initMap\" async defer></script>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "            <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/materialize.min.css"), "html", null, true);
        echo "\"  media=\"screen,projection\"/>
            <meta charset=\"utf-8\"/>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_script($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 11
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/jquery-3.3.1.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/materialize.min.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/script.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Propiedades/Home/dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 13,  267 => 12,  262 => 11,  253 => 10,  240 => 7,  234 => 5,  225 => 4,  212 => 128,  162 => 80,  159 => 79,  153 => 78,  139 => 67,  133 => 64,  128 => 62,  122 => 61,  112 => 54,  108 => 53,  98 => 50,  94 => 49,  86 => 44,  82 => 43,  78 => 42,  74 => 41,  68 => 37,  65 => 36,  60 => 35,  58 => 34,  37 => 15,  34 => 10,  32 => 4,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html lang=\"es\">
    <head>
        <title>Propiedades.com examén </title>
        {% block style %}
            <link href=\"{{ asset('bundles/propiedades/css/style.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link type=\"text/css\" rel=\"stylesheet\" href=\"{{ asset('bundles/propiedades/css/materialize.min.css') }}\"  media=\"screen,projection\"/>
            <meta charset=\"utf-8\"/>
        {% endblock style %}
        {% block script %}
            <script src=\"{{ asset('bundles/propiedades/js/jquery-3.3.1.js') }}\"></script>
            <script type=\"text/javascript\" src=\"{{ asset('bundles/propiedades/js/materialize.min.js') }}\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bundles/propiedades/js/script.js') }}\"></script>
        {% endblock script %}
    </head>
   
    <body>
        <section class=\"dashboard\">
        <div class=\"row\">
            <div class=\"col s12\">
              <ul class=\"tabs\">
                <li class=\"tab col s3\"><a href=\"#listado\">Listado de construcciones</a></li>
                <li class=\"tab col s3\"><a href=\"#alta\">Alta de construcción</a></li>
                <li class=\"tab col s3\"><a href=\"#mapa\">Mapa de construcciones</a></li>
              </ul>
            </div>
            <div id=\"listado\" class=\"col s12\">
                <div class=\"row\">
                    <div class=\"col s3\">Clave</div>
                    <div class=\"col s3\">Nombre</div>
                    <div class=\"col s3\">Delegacion</div>
                    <div class=\"col s3\"></div>
                </div>
                {% if construcciones %}
                            {% for construccion in construcciones %}
                                {% if construccion %}
                                  
                                  <ul class=\"collapsible\">
                                    <li>
                                      <div class=\"collapsible-header\">
                                          <div class=\"col s3\">{{construccion.clave}}</div>
                                          <div class=\"col s3\">{{construccion.nombre}}</div>
                                          <div class=\"col s3\">{{construccion.delegacion}}</div>
                                          <div class=\"col s3 articulo\" id=\"articulo_{{construccion.id}}\" >Ver más</div>
                                      </div>
                                      <div class=\"collapsible-body\">
                                        <article>
                                            <ul class=\"ulDescripcion\">
                                                <li>Nombre: {{construccion.nombre}}</li>
                                                <li>Dirección: {{construccion.calle}}, {{construccion.colonia}},{{construccion.delegacion}}.</li>
                                                <li>Galeria de imagenes</li>
                                                <li>
                                                    <div class=\"btn update\" id=\"id_{{construccion.id}}\">Actualizar</div>
                                                    <div class=\"btn \"><a href=\"/borrarConstruccion/{{construccion.id}}\">Eliminar</a></div>
                                                </li>
                                            </ul>
                                            <ul class=\"ulCaracteristicas\">
                                                <li class=\"col s12\">Caracteristicas</li>
                                                <li class=\"col s12\">
                                                    <div class=\"col s6 input-field\">
                                                    <input id=\"caracteristica_{{construccion.id}}\" name=\"caracteristica_{{construccion.id}}\" type=\"text\" class=\"validate\">
                                                    <label for=\"caracteristica_{{construccion.id}}\">Caracteristicas de la construcción</label>
                                                    </div>
                                                    <div class=\"btn col s2 btnCarac\" id=\"btn_{{construccion.id}}\">Agregar</div>
                                                </li>
                                                <li class=\"col s12 contentCaract\">
                                                    <ul class=\"ulContent_{{construccion.id}}\">
                                                       
                                                    </ul>
                                                </li>
                                            </ul>
                                        </article>
                                      </div>
                                    </li>

                                  </ul>
                                {% endif %}
                            {% endfor %}
                        {% endif %}
            </div>
                <div class=\"col s2\"></div>
                <div id=\"alta\" class=\"col s8\">
                    <form class=\"\" name=\"registro_construccion\" action=\"/guardar_construccion\">
                    <div class=\"input-field col s6\">
                      <input id=\"nombre\" name=\"nombre\" type=\"text\" class=\"validate\">
                      <label for=\"nombre\">Nombre</label>
                    </div>
                    <div class=\"input-field col s6\">
                      <input id=\"clave\" name=\"clave\" type=\"text\" class=\"validate\">
                      <label for=\"clave\">Clave</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"delegacion\" name=\"delegacion\" type=\"text\" class=\"validate\">
                      <label for=\"delegacion\">Delegación</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"colonia\" name=\"colonia\" type=\"text\" class=\"validate\">
                      <label for=\"colonia\">Colonia</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"calle\" name=\"calle\" type=\"text\" class=\"validate\">
                      <label for=\"calle\">Calle</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"longitud\" name=\"longitud\" type=\"text\" class=\"validate\">
                      <label for=\"longitud\">Longitud</label>
                    </div>
                    <div class=\"input-field col s12\">
                      <input id=\"latitud\" name=\"latitud\" type=\"text\" class=\"validate\">
                      <label for=\"latitud\">Latitud</label>
                    </div>
                    <div class=\"input-field col s12 center\">
                      <input type=\"hidden\" id=\"tipo\" name=\"tipo\" value=\"nuevo\">
                      <input type=\"hidden\" id=\"id\" name=\"id\" value=\"\">
                      <input type=\"submit\" value=\"Guardar\">  
                    </div>
                    </form>
                </div>
            
            <div id=\"mapa\" class=\"col s8\">
                 <div id=\"map\"></div>
            </div>
          </div>
        </section>
    </body>
</html>

<script type=\"text/javascript\" src=\"{{ asset('bundles/propiedades/js/maps.js') }}\"></script>
<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDNXSa8poK_u1zqqRfmCcu9yJ1IamYJzsA&callback=initMap\" async defer></script>
", "@Propiedades/Home/dashboard.html.twig", "C:\\xampp2\\htdocs\\fullstackP\\src\\PropiedadesBundle\\Resources\\views\\Home\\dashboard.html.twig");
    }
}
