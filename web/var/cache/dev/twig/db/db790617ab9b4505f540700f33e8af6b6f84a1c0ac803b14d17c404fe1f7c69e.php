<?php

/* @Propiedades/Home/index.html.twig */
class __TwigTemplate_fbc248a83eed4ce0897ae6312e81b8ba164af896120f9cdbe3465b298cd055af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Propiedades/Home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Propiedades/Home/index.html.twig"));

        // line 1
        echo "<html lang=\"es\">
    <head>
        <title>Propiedades.com examén </title>
        ";
        // line 4
        $this->displayBlock('style', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('script', $context, $blocks);
        // line 15
        echo "    </head>
    <body>
        <section class=\"moduleLogin\">
        <div class=\"row\">
            <div class=\"col s4\"></div>
            <div class=\"col s4\">
                
                <div class=\"input-field col s12\">
                  <input id=\"email\" type=\"text\" class=\"validate\">
                  <label for=\"email\">Email</label>
                </div>
                <div class=\"input-field col s12\">
                  <input id=\"password\" type=\"password\" class=\"validate\">
                  <label for=\"password\">Password</label>
                </div>
                <div class=\"input-field col s12 center\">
                  <input id=\"login\" type=\"submit\" value=\"Entrar\">
                </div>
                
                <div class=\"col s12 center\">
                    <a href=\"/registrate\">Registrate</a> 
                </div>
                <div class=\"col s12 center\" id=\"error\">
                    
                </div>
                
            </div>
            <div class=\"col s4\"></div>
        </div>
        </section>
    </body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "            <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/materialize.min.css"), "html", null, true);
        echo "\"  media=\"screen,projection\"/>
            <meta charset=\"utf-8\"/>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_script($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 11
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/jquery-3.3.1.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/materialize.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/script.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Propiedades/Home/index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  124 => 13,  120 => 12,  115 => 11,  106 => 10,  93 => 7,  87 => 5,  78 => 4,  37 => 15,  34 => 10,  32 => 4,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html lang=\"es\">
    <head>
        <title>Propiedades.com examén </title>
        {% block style %}
            <link href=\"{{ asset('bundles/propiedades/css/style.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link type=\"text/css\" rel=\"stylesheet\" href=\"{{ asset('bundles/propiedades/css/materialize.min.css') }}\"  media=\"screen,projection\"/>
            <meta charset=\"utf-8\"/>
        {% endblock style %}
        {% block script %}
            <script src=\"{{ asset('bundles/propiedades/js/jquery-3.3.1.js') }}\"></script>
            <script type=\"text/javascript\" src=\"{{ asset('bundles/propiedades/js/materialize.min.js') }}\"></script>
            <script type=\"text/javascript\" src=\"{{ asset('bundles/propiedades/js/script.js') }}\"></script>
        {% endblock script %}
    </head>
    <body>
        <section class=\"moduleLogin\">
        <div class=\"row\">
            <div class=\"col s4\"></div>
            <div class=\"col s4\">
                
                <div class=\"input-field col s12\">
                  <input id=\"email\" type=\"text\" class=\"validate\">
                  <label for=\"email\">Email</label>
                </div>
                <div class=\"input-field col s12\">
                  <input id=\"password\" type=\"password\" class=\"validate\">
                  <label for=\"password\">Password</label>
                </div>
                <div class=\"input-field col s12 center\">
                  <input id=\"login\" type=\"submit\" value=\"Entrar\">
                </div>
                
                <div class=\"col s12 center\">
                    <a href=\"/registrate\">Registrate</a> 
                </div>
                <div class=\"col s12 center\" id=\"error\">
                    
                </div>
                
            </div>
            <div class=\"col s4\"></div>
        </div>
        </section>
    </body>
</html>", "@Propiedades/Home/index.html.twig", "C:\\xampp2\\htdocs\\fullstackP\\src\\PropiedadesBundle\\Resources\\views\\Home\\index.html.twig");
    }
}
