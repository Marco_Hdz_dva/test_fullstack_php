<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // propiedades_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::indexAction',  '_route' => 'propiedades_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_propiedades_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'propiedades_homepage'));
            }

            return $ret;
        }
        not_propiedades_homepage:

        // propiedades_registro
        if ('/registrate' === $pathinfo) {
            return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::registrateAction',  '_route' => 'propiedades_registro',);
        }

        if (0 === strpos($pathinfo, '/g')) {
            // propiedades_guardar_registro
            if ('/guardar_registro' === $pathinfo) {
                return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::guardarRegistroAction',  '_route' => 'propiedades_guardar_registro',);
            }

            // propiedades_guardar_construccion
            if ('/guardar_construccion' === $pathinfo) {
                return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::guardarConstruccionAction',  '_route' => 'propiedades_guardar_construccion',);
            }

            if (0 === strpos($pathinfo, '/getConstruccion')) {
                // propiedades_getConstruccion
                if (preg_match('#^/getConstruccion/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_getConstruccion')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getConstrucctionAction',));
                }

                // propiedades_getConstrucciones
                if ('/getConstrucciones' === $pathinfo) {
                    return array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getConstrucctionesAction',  '_route' => 'propiedades_getConstrucciones',);
                }

            }

            // propiedades_getCaracteristicas
            if (0 === strpos($pathinfo, '/getCaracteristicas') && preg_match('#^/getCaracteristicas/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_getCaracteristicas')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getCaracteristicasAction',));
            }

        }

        // propiedades_login
        if (0 === strpos($pathinfo, '/login') && preg_match('#^/login/(?P<email>[^/]++)/(?P<password>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_login')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::loginAction',));
        }

        // propiedades_addCaracteristicas
        if (0 === strpos($pathinfo, '/addCaracteristica') && preg_match('#^/addCaracteristica/(?P<id>[^/]++)/(?P<caract>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_addCaracteristicas')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::addCaracteristicaAction',));
        }

        // propiedades_borrar_Construccion
        if (0 === strpos($pathinfo, '/borrarConstruccion') && preg_match('#^/borrarConstruccion/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_borrar_Construccion')), array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::borrarConstrucctionAction',));
        }

        // propiedades_dashboard
        if ('/dashboard' === $pathinfo) {
            return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::dashboardAction',  '_route' => 'propiedades_dashboard',);
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
