<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // propiedades_homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::indexAction',  '_route' => 'propiedades_homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_propiedades_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'propiedades_homepage'));
            }

            return $ret;
        }
        not_propiedades_homepage:

        // propiedades_registro
        if ('/registrate' === $pathinfo) {
            return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::registrateAction',  '_route' => 'propiedades_registro',);
        }

        if (0 === strpos($pathinfo, '/g')) {
            // propiedades_guardar_registro
            if ('/guardar_registro' === $pathinfo) {
                return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::guardarRegistroAction',  '_route' => 'propiedades_guardar_registro',);
            }

            // propiedades_guardar_construccion
            if ('/guardar_construccion' === $pathinfo) {
                return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::guardarConstruccionAction',  '_route' => 'propiedades_guardar_construccion',);
            }

            if (0 === strpos($pathinfo, '/getConstruccion')) {
                // propiedades_getConstruccion
                if (preg_match('#^/getConstruccion/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_getConstruccion')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getConstrucctionAction',));
                }

                // propiedades_getConstrucciones
                if ('/getConstrucciones' === $pathinfo) {
                    return array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getConstrucctionesAction',  '_route' => 'propiedades_getConstrucciones',);
                }

            }

            // propiedades_getCaracteristicas
            if (0 === strpos($pathinfo, '/getCaracteristicas') && preg_match('#^/getCaracteristicas/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_getCaracteristicas')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::getCaracteristicasAction',));
            }

        }

        // propiedades_login
        if (0 === strpos($pathinfo, '/login') && preg_match('#^/login/(?P<email>[^/]++)/(?P<password>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_login')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::loginAction',));
        }

        // propiedades_addCaracteristicas
        if (0 === strpos($pathinfo, '/addCaracteristica') && preg_match('#^/addCaracteristica/(?P<id>[^/]++)/(?P<caract>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_addCaracteristicas')), array (  '_controller' => 'PropiedadesBundle\\Controller\\AjaxController::addCaracteristicaAction',));
        }

        // propiedades_borrar_Construccion
        if (0 === strpos($pathinfo, '/borrarConstruccion') && preg_match('#^/borrarConstruccion/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'propiedades_borrar_Construccion')), array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::borrarConstrucctionAction',));
        }

        // propiedades_dashboard
        if ('/dashboard' === $pathinfo) {
            return array (  '_controller' => 'PropiedadesBundle\\Controller\\HomeController::dashboardAction',  '_route' => 'propiedades_dashboard',);
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
