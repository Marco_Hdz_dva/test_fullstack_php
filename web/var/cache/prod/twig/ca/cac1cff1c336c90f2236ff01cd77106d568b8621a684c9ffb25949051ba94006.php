<?php

/* @Propiedades/Home/index.html.twig */
class __TwigTemplate_53f6c2c6bac02bcedb16d39193fb20e8c90a8045d01f0b709bd0be0e90f399ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"es\">
    <head>
        <title>Propiedades.com examén </title>
        ";
        // line 4
        $this->displayBlock('style', $context, $blocks);
        // line 10
        echo "        ";
        $this->displayBlock('script', $context, $blocks);
        // line 15
        echo "    </head>
    <body>
        <section class=\"moduleLogin\">
        <div class=\"row\">
            <div class=\"col s4\"></div>
            <div class=\"col s4\">
                
                <div class=\"input-field col s12\">
                  <input id=\"email\" type=\"text\" class=\"validate\">
                  <label for=\"email\">Email</label>
                </div>
                <div class=\"input-field col s12\">
                  <input id=\"password\" type=\"password\" class=\"validate\">
                  <label for=\"password\">Password</label>
                </div>
                <div class=\"input-field col s12 center\">
                  <input id=\"login\" type=\"submit\" value=\"Entrar\">
                </div>
                
                <div class=\"col s12 center\">
                    <a href=\"/registrate\">Registrate</a> 
                </div>
                <div class=\"col s12 center\" id=\"error\">
                    
                </div>
                
            </div>
            <div class=\"col s4\"></div>
        </div>
        </section>
    </body>
</html>";
    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        // line 5
        echo "            <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/css/materialize.min.css"), "html", null, true);
        echo "\"  media=\"screen,projection\"/>
            <meta charset=\"utf-8\"/>
        ";
    }

    // line 10
    public function block_script($context, array $blocks = array())
    {
        // line 11
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/jquery-3.3.1.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/materialize.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/propiedades/js/script.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "@Propiedades/Home/index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  90 => 12,  85 => 11,  82 => 10,  75 => 7,  69 => 5,  66 => 4,  31 => 15,  28 => 10,  26 => 4,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Propiedades/Home/index.html.twig", "C:\\xampp2\\htdocs\\fullstackP\\src\\PropiedadesBundle\\Resources\\views\\Home\\index.html.twig");
    }
}
